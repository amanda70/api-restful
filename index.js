const express = require('express');
const app = express();
app.use(express.json());
const port = 5005;
var datos = {};

app.get('/estudiente', (req, res) => {
    res.json({
        "datos": {
            "estudiante": {
                "apellidos": "Arguedas Amaya",
                "celular": 63868971,
                "email": "rarguedasa@est.utn.ac.cr",
                "id": 117330997,
                "nivel": "diplomado",
                "nombre": "Amanda Arguedas"
            },
            "status": 200,
            "status_message": "OK"
        }
    })
});

app.post('/datos', (req, res) => {
    datos = req.body
    var dbLength = Object.keys(datos).length;
    res.json({
        "resultado": {
            "datos": {
                "nValores": dbLength
            },
            "status": 201,
            "status_message": "Data created"
        }
    })
});

app.get('/estadisticas', (req, res) => {
    var estadisticas = Object.values(datos);
    res.json({
        "resultado": {
            datos,
            "estadisticas": {
                "mayor": Math.max(...estadisticas),
                "menor": Math.min(...estadisticas),
                "nValores": estadisticas.length,
                "promedio": Proimedio(estadisticas),
                "suma": estadisticas.reduce((a, b) => a + b, 0)
            },
            "status": 200,
            "status_message": "OK"
        }
    });
});

app.get('/elevar', (req, res) => {
    var d_originales = datos;
    res.json({
        "resultado": {
            "d_elevados": {
                "a": Math.pow(d_originales.a, 2),
                "b": Math.pow(d_originales.b, 2),
                "c": Math.pow(d_originales.c, 2),
                "d": Math.pow(d_originales.d, 2)
            },
            d_originales,
            "status": 200,
            "status_message": "OK"
        }
    });
});

app.get('/elevar/:number', (req, res) => {
    var d_originales = datos;
    res.json({
        "resultado": {
            "d_elevados": {
                "a": Math.pow(d_originales.a, req.params.number),
                "b": Math.pow(d_originales.b, req.params.number),
                "c": Math.pow(d_originales.c, req.params.number),
                "d": Math.pow(d_originales.d, req.params.number)
            },
            d_originales,
            "status": 200,
            "status_message": "OK"
        }
    });
});

app.get('/entero/:number', (req, res) => {
    const CantiadDebits = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24];
    var Representacion = [];
    for (var i = 0; i < CantiadDebits.length; i++) {
        Representacion.push(Math.pow(req.params.number, CantiadDebits[i]))
        if (CantiadDebits.length - 1 == i) {
            res.json({
                "resultado": {
                    "con_signo": `${(Representacion[15] / 2) * -1} hasta ${(Representacion[15] / 2) - 1}`,
                    "sin_signo": `0 hasta el ${Representacion[15] - 1}`,
                    "n_bits": 16
                },
                "status": 200,
                "status_message": "OK"
            });
        }
    }
});


function Proimedio(Array) {
    var i = 0, summ = 0, ArrayLen = Array.length;
    while (i < ArrayLen) {
        summ = summ + Array[i++];
    }
    return summ / ArrayLen;
}


app.listen(port, () => {
    console.log(`Listening on port ${port}`);
});